package com.indramahkota.ane.toast
{
    import com.indramahkota.ane.utils.StringUtil;
    import com.indramahkota.ane.utils.SystemUtil;

    import flash.events.ErrorEvent;
    import flash.events.EventDispatcher;
    import flash.events.StatusEvent;
    import flash.external.ExtensionContext;

    public class ToastExtension extends EventDispatcher
    {
        public static const EXTENSION_ID:String = "com.indramahkota.ane.Toast";

        protected var _context:ExtensionContext;

        public function ToastExtension()
        {
            if (!isSupported)
                throw new Error("ToastExtension is not supported on this platform. Use ToastExtension.isSupported getter.");
            _context = ExtensionContext.createExtensionContext(EXTENSION_ID, null);
            _context.addEventListener(StatusEvent.STATUS, onStatusEventHandler);
        }

        public function show():void
        {
            _context.call("show");
        }

        public function setText(text:String):void
        {
            if (StringUtil.isTextValid(text))
                _context.call("setText", text);
            else
                throw new ArgumentError("Parameter text is not valid");
        }

        public function setDuration(duration:DurationEnum):void
        {
            _context.call("setDuration", duration.value);
        }

        public function cancel():void
        {
            _context.call("cancel");
        }

        public function dispose():void
        {
            if (_context != null)
            {
                _context.removeEventListener(StatusEvent.STATUS, onStatusEventHandler);
                _context.dispose();
                _context = null;
            }
        }

        protected function onStatusEventHandler(event:StatusEvent):void
        {
            const ERROR_EVENT:String = "error_event";

            switch (event.level)
            {
                case ERROR_EVENT:
                    dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, event.code));
                    break;
                default:
                    dispatchEvent(event);
            }
        }

        public static function get isSupported():Boolean
        {
            return SystemUtil.isAndroid();
        }
    }
}
