package com.indramahkota.ane.utils
{
    import flash.system.Capabilities;

    public class SystemUtil
    {
        public static function isAndroid():Boolean
        {
            return Capabilities.manufacturer.indexOf("Android") > -1;
        }
    }
}
