package com.indramahkota.toast;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREExtension;

public class ToastExtension implements FREExtension {

    public FREContext context;

    @Override
    public FREContext createContext(String extId) {
        return context = new ToastExtensionContext();
    }

    @Override
    public void initialize() {}

    @Override
    public void dispose() {
        if (context != null) {
            context.dispose();
            context = null;
        }
    }
}
